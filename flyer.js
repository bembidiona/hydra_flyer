// -- P5 Init
p5.remove() // La primera vez que lo corres dejar esta linea comentada
p5 = new P5()
s0.init({src: p5.canvas})

size_width = 800
size_height = 800
// p5.hide()
// -- P5 Setup
p5.fill(255, 0, 100)
p5.imageMode(p5.CENTER)
p5.textFont('Consolas')
// -- P5 Draw
logo_fna = p5.loadImage("https://gitlab.com/bembidiona/hydra_flyer/-/raw/master/assets/fna.png")
logo_invap = p5.loadImage("https://gitlab.com/bembidiona/hydra_flyer/-/raw/master/assets/invap.png")
let margin = size_width/15
let COLOR_TXT_STROKE = p5.color(20,0,20,255)
let COLOR_TXT_BG = p5.color(0,0,0,150)
let liney = margin*2;
let linex_L = margin;
let linex_R = size_width - linex_L;
let h1 = 85
let h2 = 40
let h3 = 30
function drawtxt(txt,x,y,size, align)
{
    let txtbg_size = txt.length*size*0.55
    let txtbg_x = x
    if(align == -1)
    {
        p5.textAlign(p5.LEFT);
    }
    else if(align == 0)
    {
        p5.textAlign(p5.CENTER);
        txtbg_x -= txtbg_size/2
    }
    else if(align == 1)
    {
        p5.textAlign(p5.RIGHT);
        txtbg_x -= txtbg_size
    }
    p5.noStroke()
    p5.fill(COLOR_TXT_BG)
    p5.rect(txtbg_x,y-size*0.7,txtbg_size,size*0.8)
    let color;
    p5.stroke(COLOR_TXT_STROKE)
    if(size == h1)
    {
        p5.strokeWeight(10)
        color = p5.color(255,p5.random(255),255)
        p5.textStyle(p5.BOLD)
        x += Math.sin(time)*3;
        y += Math.cos(time)*6;
    }
    else if(size == h2)
    {
        p5.strokeWeight(6)
        color = p5.color(255)
        p5.textStyle(p5.NORMAL)
        x += Math.sin(time)*4;
    }
    else if (size == h3)
    {
        p5.strokeWeight(4)
        color = p5.color(255)
        p5.textStyle(p5.ITALIC)
        // x += Math.sin(time)*4;
    }
    p5.textSize(size)
    p5.fill(0)
    p5.text(txt, x, y+2);
    p5.fill(color)
    p5.text(txt, x, y);
    liney += size-2
}
function drawlogo(img,x,y)
{
    p5.tint(COLOR_TXT_STROKE)
    p5.image(img, x, y+3);
    // p5.image(img, x, y+4);
    // p5.image(img, x, y+8);
    // p5.image(img, x+4, y);
    // p5.image(img, x-4, y);
    p5.noTint()
    p5.image(img, x, y);
}
p5.draw = () => {
    liney = margin*2.5;
    p5.clear()
    //--- LEFT ---
    drawtxt('WORKSHOP HYDRA', linex_L, liney, h1, -1);
    drawtxt('programacion de visuales en vivo', linex_L, liney, h2, -1);
    drawtxt('', linex_L, liney, h1, -1);
    drawtxt('Bariloche', linex_R, liney, h2, 1);
    drawtxt('Espacio Soria Moria', linex_R, liney, h3, 1);
    drawtxt('Marzo 20', linex_R, liney, h3, 1);
    drawtxt('16:00', linex_R, liney, h3, 1);
    drawtxt('', linex_R, liney, h3, 1);
    drawtxt('GRATIS', linex_R, liney, h2, 1);
    //---logos
    let logos_y = size_height - margin*2;
    drawlogo(logo_fna, size_width/2 - 160 + Math.sin(time*2)*5, logos_y);
    drawlogo(logo_invap, size_width/2 + 160 + Math.sin(time*2+0.5)*5, logos_y);
    //rect canvas
    p5.noFill()
    p5.stroke(COLOR_TXT_STROKE)
    p5.strokeWeight(2)
    p5.rect(10, 10, size_width, size_height)
    //marquee cheto
    p5.stroke(255,200)
    p5.strokeWeight(1)
    let dist = 10;
    for(var i=0;i<4;i++)
    {
        p5.rect(i*dist, i*dist, size_width-i*dist*2, size_height-i*dist*2)
    }
}
//
//--------------------------------------------------------------------
//--------------------------------------------------------------------
                    // Logo FNA
                    // let fna_scale = 0.5
                    // shape(4).scale(0.4, 0.4).add(shape(4).scale(0.4, 0.9, 0.4).scrollX(-0.03).scrollY(0.04), 1).scrollX(0.15).color(0,1,0)
                    // .add(shape(3).scale(0.4, 0.4, -0.7).scrollY(-0.02).scrollX(-0.15).color(0,0,1),1)
                    // .add(shape(20).scale(0.5).mask(shape(20).scale(0.3).invert()).mask(shape(2).scale(0.3).scrollY(0.045)).scrollY(-0.06).color(1,0,0),1)
                    // .add(shape(4).scale(0.5, 2.5,0.15).scrollY(-0.08).scrollX(-0.005).color(1,1,0),1)
                    // .scale(fna_scale)
                    // .scale(()=> 1 + (Math.sin(time)*0.1))
                    // .mask(shape(4).scale(fna_scale, 1.5,1))
                    // .out(o2)
                    // Logo INVAP levanta imagen
                    // var logo_invap = document.createElement('img'); // <img />
                    // let logo_invap_scale = 0.25
                    // logo_invap.src = 'https://i.imgur.com/WynSNkD.png';    // <img src=" " />
                    // s1.init({src: logo_invap, dynamic: false});
// ----------------------------------------
// --- Main Hydra ---
// ----------------------------------------
shape(2).scrollY(-0.2).rotate(0,1)
.mult(osc(10,0.1,()=>Math.sin(2*time)*1).color(0.8,1,0.2).contrast(1).kaleid(200))
.contrast(1.3)
.modulateRepeatX(osc(3,0.5),2)
.modulateRotate(osc(3,0.1),2)
.brightness(0.1)
.diff(src(o0).scrollX(0.001).scrollY(-0.001).scale([1,1.01,1.1]))
.blend(src(o0),0.8)
.modulate(src(o0).scrollX(0.001),()=>Math.sin(time*0.25)*0.01)
.rotate(0.005)
.scale(1.004)
.colorama(0.005)
.luma(0.08)
.diff(src(s0).scale(1.05).scrollX(()=>Math.sin(time*2)*0.01)) // s0 tiene el texto y los logos
// .add(s0)
.out(o0)
